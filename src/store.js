import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        modal: false,
        header: false,
        websiteDataLoaded: false,
        websiteData: {type: Object, default:['loading...']}
    },
    mutations: {
        showModal(state) {
            state.modal = true;
        },
        hideModal(state) {
            state.modal = false;
        },
        toggleHeader(state, value) {
            state.header = !!value;
        },
        websiteDataLoaded(state) {
            state.websiteDataLoaded = true;
        },
        setData(state, value) {
            state.websiteData = value;
        },
    },
    actions: {},
    getters: {
        getPageBySlug: (state) => (slug) => {
            return state.websiteData.pages.find(page => page.slug === slug)
        },
        getCitizenshipBySlug: (state) => (slug) => {
            return state.websiteData.citizenships.find(citizenship => citizenship.slug === slug)
        },
        getBlogBySlug: (state) => (slug) => {
            return state.websiteData.blog_posts.find(blog => blog.slug === slug)
        }
    },
});